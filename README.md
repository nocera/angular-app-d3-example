# MyApp

Angular app to demonstrate routing and passing data to charts using `@input` decorator for re-usable charts.

## Routing

The app is created with `ng new my-app` and answering `yes` to routing question to create `src/app/app-routing.module.ts`. 

After creating components for `home` and `link` we add routes in `src/app/app-routing.module.ts`:

```
...

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'link', component: LinkComponent }
];

...
```

and in `home` and `link` we use the `routerLink` directive to route as needed, e.g., in `home.component.html`:

```
...

	<ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" routerLink="/home">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" routerLink="/link">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>

...

```

## Re-usable chart

To implement re-usable charts with d3 in angular you will want to pass data from the parent component to the chart component. Then the chart can be used to implement small-multiples.

This is done using the `@input` decorator to specify the `data` in the chart component `char.component.ts`:

```
...

    @Input() data: any[];
	
...
```

then in `home.component.html` template (datasets is an array of data arrays declared in `home.component.ts`):

```
    <h1>Passing data into the chart directly</h1>
    <app-chart [data]="[1, 2, 3]"></app-chart>
    <app-chart [data]="[4, 5, 6]"></app-chart>

    <h1>Passing data into the chart directly from parent data</h1>
    <app-chart [data]="datasets[0]"></app-chart>
    <app-chart [data]="datasets[0]"></app-chart>

    <h1>Passing data into the chart from parent datasets with *ngFor</h1>
    <div *ngFor="let d of datasets">
      <app-chart [data]="d"></app-chart>
    </div>

    <h1>Passing data into the chart from parent datasets with *ngFor in bootstrap grid</h1>
    <div class="container">
    <div class="row">
      <div class="col-sm" *ngFor="let d of datasets">
          <app-chart [data]="d"></app-chart>
      </div>
    </div>
  </div>
```

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

This is an example of using routes and creating small multiples with d3. The app was create with `ng new my-app` and routes. Components were added with `ng generate component`, e.g., `ng generate component link`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
