import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges} from '@angular/core';
import * as d3 from 'd3';

//https://auth0.com/blog/real-time-charts-using-angular-d3-and-socket-io/
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @ViewChild('chart')
  chartElement: ElementRef;
  @Input() data: any[];

  constructor() { }

  ngOnInit() {
    const svg = d3.select(this.chartElement.nativeElement);
    svg.attr('background-color', 'orange');

    console.log(svg);
    console.log(this.data);

    svg.selectAll('rect')
      .data(this.data)
      .enter()
      .append('rect')
      .attr('y', function (d, i) { return i * 25; })
      .attr('width', function (d) { console.log(d); return d * 100; })
      .attr('height', 20)
      .attr('class', 'bar');
  }

  ngOnChanges() { }
}
